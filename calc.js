/**
 * Created by quinnc on 4/21/2016.
 */

var _ = require('lodash');

var yearlySalary = 100000; // 年薪
var housingFund = 0; // 公积金

// 打印全部细节
var printDetail = false;

var NO_TAX_DEDUCT = 3500;
var taxRateTable = [
    {
        taxableIncome: {
            min: -9999999,
            max: 0
        },
        rate: 0,
        rapidCalcDeduct: 0
    }, {
        taxableIncome: {
            min: 0,
            max: 1500
        },
        rate: 0.03,
        rapidCalcDeduct: 0
    }, {
        taxableIncome: {
            min: 1500,
            max: 4500
        },
        rate: 0.10,
        rapidCalcDeduct: 105
    }, {
        taxableIncome: {
            min: 4500,
            max: 9000
        },
        rate: 0.20,
        rapidCalcDeduct: 555
    }, {
        taxableIncome: {
            min: 9000,
            max: 35000
        },
        rate: 0.25,
        rapidCalcDeduct: 1005
    }, {
        taxableIncome: {
            min: 35000,
            max: 55000
        },
        rate: 0.30,
        rapidCalcDeduct: 2755
    }, {
        taxableIncome: {
            min: 55000,
            max: 80000
        },
        rate: 0.35,
        rapidCalcDeduct: 5505
    }, {
        taxableIncome: {
            min: 80000,
            max: 9999999999999999
        },
        rate: 0.45,
        rapidCalcDeduct: 13505
    }
];


var results = [];
var monthlySalary = yearlySalary / 12;

// 以1块为单位穷举所有可能的搭配
while (monthlySalary > 0) {
    var tax = {
        monthSalary: monthlySalary,
        yearBonus: yearlySalary - monthlySalary * 12,
        monthTax: 0,
        yearBonusTax: 0,
        totalTax: 0
    };

    // 月薪计税
    var taxableIncome = monthlySalary - NO_TAX_DEDUCT - housingFund;
    var monTaxRate = _.find(taxRateTable, function (x) {
        return taxableIncome > x.taxableIncome.min && taxableIncome <= x.taxableIncome.max
    });
    tax.monthTax = taxableIncome * monTaxRate.rate - monTaxRate.rapidCalcDeduct;

    // 年终奖计税
    var bonusPerMon = tax.yearBonus / 12;
    var bonusTaxRate = _.find(taxRateTable, function (x) {
        return bonusPerMon > x.taxableIncome.min && bonusPerMon <= x.taxableIncome.max
    });
    tax.yearBonusTax = tax.yearBonus * bonusTaxRate.rate - bonusTaxRate.rapidCalcDeduct;

    tax.totalTax = tax.monthTax * 12 + tax.yearBonusTax;
    results.push(tax);

    // 减去1块，进入下一搭配
    monthlySalary -= 1;
}

// 找到最低税额
var lowestTax;
_.each(results, function (tax) {
    if (lowestTax === undefined || tax.totalTax < lowestTax.totalTax) {
        lowestTax = tax;
    }
});

// 打印全部结果
if (printDetail) {
    console.log('-------------全部配置-------------------');
    _.each(results, function (tax) {
        prettyPrint(tax);
    });
}

// 打印最优配置
console.log('-------------最优配置-------------------');
prettyPrint(lowestTax);


function prettyPrint(tax) {
    var monInHandSalary = tax.monthSalary - tax.monthTax - housingFund;
    var yearBonusInHandSalary = tax.yearBonus - tax.yearBonusTax;

    console.log(`
    月薪(税前): ${tax.monthSalary.toFixed(2)}, \t 
    年终奖(税前): ${tax.yearBonus.toFixed(2)}, \t
    月薪所得税: ${tax.monthTax.toFixed(2)},  \t
    年终奖所得税: ${tax.yearBonusTax.toFixed(2)}, \t
    全年税额: ${tax.totalTax.toFixed(2)}, \t
    月薪(到手): ${monInHandSalary.toFixed(2) }, \t
    年终奖(到手): ${yearBonusInHandSalary}, \t
    公积金收入: ${housingFund.toFixed(2)}, \t
    全部到手收入（含公积金）: ${((monInHandSalary + housingFund) * 12 + yearBonusInHandSalary).toFixed(2)}`);
}